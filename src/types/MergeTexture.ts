export default interface MergeTexture {
  texture?: string | null;
  color?: number;
  x?: number;
  y?: number;
}
