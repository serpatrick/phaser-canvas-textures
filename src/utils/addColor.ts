import { OUTLINE_PLACEHOLDER_COLOR } from '../consts.js';
import hexToString from './hexToString.js';

export default function (canvas: Phaser.Textures.CanvasTexture, color: number) {
  const context = canvas.getContext();
  const { width, height } = canvas.canvas;
  const pixel = new Phaser.Display.Color();

  canvas.update();

  context.globalCompositeOperation = 'multiply';
  context.fillStyle = hexToString(color);

  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      canvas.getPixel(x, y, pixel);

      if (pixel.color === OUTLINE_PLACEHOLDER_COLOR) {
        continue;
      }

      if (pixel.alpha === 0) {
        continue;
      }

      context.fillRect(x, y, 1, 1);
    }
  }
}
