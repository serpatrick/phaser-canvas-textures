export default function (hex: number) {
  const str = hex.toString(16).padStart(6, '0');
  return `#${str}`;
}
