import { OUTLINE_PLACEHOLDER_COLOR } from '../consts.js';
import hexToString from './hexToString.js';

function isPixelValid(
  canvas: Phaser.Textures.CanvasTexture,
  x: number,
  y: number,
  out: Phaser.Display.Color
) {
  if (x < 0 || y < 0 || x >= canvas.width || y >= canvas.height) {
    return false;
  }

  const pixel = canvas.getPixel(x, y, out);

  if (pixel.color === OUTLINE_PLACEHOLDER_COLOR) {
    return false;
  }

  if (pixel.alpha < 255) {
    return false;
  }

  return true;
}

export default function (
  canvas: Phaser.Textures.CanvasTexture,
  color: number,
  clear: boolean
) {
  const context = canvas.getContext();
  const { width, height } = canvas.canvas;
  const pixel = new Phaser.Display.Color();
  const pixels = [];

  canvas.update();

  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      canvas.getPixel(x, y, pixel);

      if (pixel.color === OUTLINE_PLACEHOLDER_COLOR) {
        pixels.push([x, y]);
        continue;
      }

      if (pixel.alpha !== 0) {
        continue;
      }

      const l = isPixelValid(canvas, x - 1, y, pixel);
      const r = isPixelValid(canvas, x + 1, y, pixel);
      const u = isPixelValid(canvas, x, y - 1, pixel);
      const d = isPixelValid(canvas, x, y + 1, pixel);

      if (l || r || u || d) {
        pixels.push([x, y]);
      }
    }
  }

  if (clear) {
    canvas.clear();
  }

  context.fillStyle = hexToString(color);

  for (const [x, y] of pixels) {
    context.fillRect(x, y, 1, 1);
  }
}
