import { OUTLINE_PLACEHOLDER_COLOR } from '../consts.js';

export default function (
  canvas: Phaser.Textures.CanvasTexture,
  x: number,
  y: number,
  out?: Phaser.Display.Color
) {
  const pixel = canvas.getPixel(x, y, out);

  if (pixel.color === OUTLINE_PLACEHOLDER_COLOR) {
    return false;
  }

  if (pixel.alpha === 0) {
    return false;
  }

  return true;
}
