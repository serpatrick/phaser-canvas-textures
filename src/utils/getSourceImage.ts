export default function (
  scene: Phaser.Scene,
  key: string | Phaser.Textures.Texture
) {
  return scene.textures.get(key).getSourceImage() as HTMLImageElement;
}
