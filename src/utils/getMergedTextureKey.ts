import MergeTexture from '../types/MergeTexture.js';

export default function (textures: MergeTexture[]) {
  return textures.map((texture) => Object.values(texture).join('')).join('');
}
