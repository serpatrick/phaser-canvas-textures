import 'phaser';
export { default as createMergedTexture } from './lib/createMergedTexture.js';
export { default as createOutlinedTexture } from './lib/createOutlinedTexture.js';
export { default as createOutlineTexture } from './lib/createOutlineTexture.js';
export { default as createTexture } from './lib/createTexture.js';
export { default as createTexturesFromAtlas } from './lib/createTexturesFromAtlas.js';
export { default as MergeTexture } from './types/MergeTexture.js';
export * from './consts.js';
