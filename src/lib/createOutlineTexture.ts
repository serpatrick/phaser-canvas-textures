import createTexture from './createTexture.js';
import addOutline from '../utils/addOutline.js';
import getSourceImage from '../utils/getSourceImage.js';

interface Options {
  scene: Phaser.Scene;
  texture: string | Phaser.Textures.CanvasTexture;
  color: number;
}

export default function ({ scene, texture, color }: Options) {
  const image = getSourceImage(scene, texture);

  const keyPostfix =
    texture instanceof Phaser.Textures.CanvasTexture ? texture.key : texture;

  const [canvas, cached] = createTexture({
    scene,
    width: image.width,
    height: image.height,
    key: `outline_${keyPostfix}`,
  });

  if (cached) {
    return canvas;
  }

  canvas.draw(0, 0, image);
  addOutline(canvas, color, true);
  canvas.refresh();

  return canvas;
}
