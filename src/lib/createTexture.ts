interface Options {
  scene: Phaser.Scene;
  key: string;
  width: number;
  height: number;
}

export default function ({ scene, key, width, height }: Options) {
  let canvas: Phaser.Textures.CanvasTexture;
  let cached: boolean;

  if (scene.textures.exists(key)) {
    const texture = scene.textures.get(key);

    if (!(texture instanceof Phaser.Textures.CanvasTexture)) {
      throw new Error(
        `Texture with key ${key} is not of type Phaser.Textures.CanvasTexture`
      );
    }

    canvas = texture;
    cached = true;
  } else {
    canvas = scene.textures.createCanvas(key, width, height);
    cached = false;
  }

  return [canvas, cached] as [Phaser.Textures.CanvasTexture, boolean];
}
