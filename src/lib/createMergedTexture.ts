import createTexture from './createTexture.js';
import addOutline from '../utils/addOutline.js';
import addColor from '../utils/addColor.js';
import getSourceImage from '../utils/getSourceImage.js';
import MergeTexture from '../types/MergeTexture.js';
import getMergedTextureKey from '../utils/getMergedTextureKey.js';
import addShadow from '../utils/addShadow.js';

interface Options {
  scene: Phaser.Scene;
  key?: string;
  textures: MergeTexture[];
  outline?: number;
  shadow?: number;
}

export default function ({ scene, key, textures, outline, shadow }: Options) {
  const [first] = textures;

  if (!first || !first.texture) {
    throw new Error('Should contain at least one object with a texture');
  }

  if (!key) {
    key = getMergedTextureKey(textures);
  }

  const { width, height } = getSourceImage(scene, first.texture);

  const [canvas, cached] = createTexture({
    scene,
    width,
    height,
    key: `merged_${key}`,
  });

  if (cached) {
    return canvas;
  }

  const [buffer] = createTexture({
    scene,
    width,
    height,
    key: `buffer_${width}x${height}`,
  });

  for (const { x = 0, y = 0, texture, color } of textures) {
    if (!texture) {
      continue;
    }

    const image = getSourceImage(scene, texture);

    buffer.clear();
    buffer.draw(x, y, image);

    if (color !== undefined) {
      addColor(buffer, color);
    }

    canvas.draw(0, 0, getSourceImage(scene, buffer));
  }

  if (shadow !== undefined) {
    addShadow(canvas, shadow);
  }

  if (outline !== undefined) {
    addOutline(canvas, outline, false);
  }

  canvas.refresh();

  return canvas;
}
