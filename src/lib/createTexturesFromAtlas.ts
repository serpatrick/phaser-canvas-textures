import createTexture from './createTexture.js';
import getSourceImage from '../utils/getSourceImage.js';

interface Frame {
  name: string;
  x: number;
  y: number;
}

export default function (
  scene: Phaser.Scene,
  texture: string,
  width: number,
  height: number,
  frames: Frame[]
) {
  const image = getSourceImage(scene, texture);

  for (const { name, x, y } of frames) {
    const key = `${texture}_${name}`;
    const [canvas, cached] = createTexture({ scene, key, width, height });

    if (cached) {
      continue;
    }

    const context = canvas.getContext();

    context.drawImage(
      image,
      x * width,
      y * height,
      width,
      height,
      0,
      0,
      width,
      height
    );

    canvas.refresh();
  }
}
